import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:http/http.dart' as http;


class Newuser extends StatefulWidget {

  @override
  State<Newuser> createState() => _NewuserState();
}

class _NewuserState extends State<Newuser> {

  var formKey = GlobalKey<FormState>();

  var nameControler = TextEditingController();

  var rnameControler = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Colors.black,
                      Colors.grey,
                    ],
                    stops: [
                      0.0,
                      1.0
                    ],
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter,
                    tileMode: TileMode.repeated)),

            child: SafeArea(
              child: Column(
                children: [
                  SizedBox(height: 10.h,),
                  Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 5.h,left: 2.w,right: 2.w),
                          child: Column(
                            children: [
                              TextFormField(
                                decoration: InputDecoration(labelText: 'enter new name'),
                                validator: (value) {
                                  if (value != null && value.isEmpty) {
                                    return "enter valid name";
                                  }
                                },
                                controller: nameControler,
                              ),
                              SizedBox(height: 2.h,),
                              TextFormField(
                                decoration: InputDecoration(labelText: 'enter new email'),
                                validator: (value) {
                                  if (value != null && value.isEmpty) {
                                    return "enter valid email";
                                  }
                                },
                                controller: rnameControler,
                              ),
                              SizedBox(height: 2.h,),
                              Container(
                                padding: EdgeInsets.only(top: 3.h),
                                child: TextButton(
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStatePropertyAll(Colors.black)
                                    ),
                                    onPressed: () {
                                      if (formKey.currentState!.validate()) {
                                        insertUser().then((value) => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Newuser()),));
                                      }
                                    },
                                    child: Text('submit',style: TextStyle(color: Colors.white,fontSize: 18.sp),)),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
  Future<void> insertUser() async {
    Map map = {};
    map["Name"] = nameControler.text;
    map["RName"] = rnameControler.text;
    var response1 = await http.post(
        Uri.parse('https://63eb9a4f3f583963b2e10df2.mockapi.io/foodItems'),body: map);
    print(response1.body);
  }
}
