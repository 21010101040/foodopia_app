import 'package:flutter/material.dart';
import 'package:foodopia/Main_Pages/home.dart';
import 'package:foodopia/Main_Pages/order.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:expandable_text/expandable_text.dart';


class BlackBurger extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        body: Stack(
          children: [
            Positioned(
                left: 0.h,
                right: 0.h,
                child: Container(
                  width: double.infinity,
                  height: 50.h,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                        image: AssetImage("assets/images/blackburger2.jpg"))
                  ),
                ),
            ),
            Positioned(
              left: 1.w,
              right: 1.w,
              top: 1.h,
              bottom: 87.h,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 4.w),
                    child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: 22.sp,
                    ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Home())); },
            ),
                  ),
                  SizedBox(width: 67.w,),
                  Container(
                    child: CircleAvatar(
                      backgroundColor: Colors.white38,
                      child: IconButton(
                        icon: Icon(
                          Icons.more_horiz,
                          color: Colors.white,
                          size: 20.sp,
                        ), onPressed: () {},
                      ),
                    ),
                  ),
                ],
              ),),
            Positioned(
              left: 0.h,
              right: 0.h,
              top: 40.h,
              child: Container(
                height: 60.h,
                decoration: BoxDecoration(
                    color: Colors.white,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(40),topLeft: Radius.circular(40)),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 3.h),
                    Container(
                      height: 6.h,
                      width: 85.w,
                      color: Colors.white,
                      child: Row(
                        children: [
                          SizedBox(width: 1.w),
                          Container(
                            height:5.h,
                            width: 12.w,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/mcd.png')
                                )
                            ),
                          ),
                          SizedBox(width: 2.w),
                          Container(
                            margin: EdgeInsets.only(top: 1.h),
                            child: Column(
                              children: [
                                Container(
                                  child: Text("McDonald's",style: TextStyle(fontWeight: FontWeight.bold),),
                                ),
                                Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text("3.5 KM",style: TextStyle(fontSize: 13.sp,color: Colors.grey),),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 1.h),
                    Container(
                      padding: EdgeInsets.only(left: 8.w,top: 2.h),
                      height: 7.h,
                      width: 100.w,
                      child: Text("Black Burger",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22.sp),),
                    ),
                    SizedBox(height: 1.h,),
                    Container(
                      padding: EdgeInsets.only(left: 8.w),
                      height: 3.h,
                      width: 100.w,
                      // color: Colors.cyan,
                      child: Row(
                        children: [
                          Icon(Icons.star,size: 20.sp,color: Color.fromARGB(255, 229, 164, 2),),
                          Icon(Icons.star,size: 20.sp,color: Color.fromARGB(255, 229, 164, 2),),
                          Icon(Icons.star,size: 20.sp,color: Color.fromARGB(255, 229, 164, 2),),
                          Icon(Icons.star,size: 20.sp,color: Color.fromARGB(255, 229, 164, 2),),
                          Icon(Icons.star,size: 20.sp,color: Color.fromARGB(255, 229, 164, 2),),

                        ],
                      ),
                    ),
                    Container(
                      height: 7.h,
                      width: 100.w,
                      child: Row(
                        children: [
                          SizedBox(width:8.w),
                          Icon(Icons.currency_rupee,size: 15.sp,),
                          Text("150",style: TextStyle(fontSize: 20.sp),),
                          SizedBox(width: 55.w),
                          Icon(Icons.remove),
                          SizedBox(width:1.w),
                          Text("1"),
                          SizedBox(width:1.w),
                          Icon(Icons.add),
                        ],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 1.h,right: 3.w,left: 3.w),
                        height: 20.h,
                        width: 100.w,
                        child: SingleChildScrollView(
                          child: ExpandableText(
                              "We've all heard about how the Black Burger became a rage in USA and Japan. This quirky burger had foodies curious as to how the taste would be! No more wondering Dilli Walon. Hop on over to their outlet in Khan Market and give it a try! Barcelos has introduced two variants of Black Burger i.e. Peri Black Beauty (Veg) priced at Rs155 and Peri Black Magic (Chicken) priced at Rs195. Curious about why it's black?",
                            style: TextStyle(fontSize: 15.sp),
                            expandText: 'Read More',
                            collapseText: 'Show Less',
                            maxLines: 6,
                            linkColor: Colors.orange,),
                        ),
                      ),
                    Container(
                      height: 7.h,
                      width: 70.w,
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)
                                  )
                              ),
                              backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 229, 164, 2))
                          ),
                          onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Order()));},
                          child: Center(
                              child: Text("Place an Order",style: TextStyle(color: Colors.white),)
                          )
                      ),
                    ),
                  ],
                ),
              ),),

          ],
        ),
      );
    });
    }
}