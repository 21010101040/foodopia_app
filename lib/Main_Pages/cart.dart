import 'package:flutter/material.dart';

class Cart extends StatelessWidget{
  const Cart({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.orangeAccent,
        child: Center(child: Text("Added To Cart",style: TextStyle(fontWeight: FontWeight.bold),)),
      ),
    );
  }

}