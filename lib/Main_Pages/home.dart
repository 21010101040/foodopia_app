import 'package:flutter/material.dart';
import 'package:foodopia/Food_Items/black_burger.dart';
import 'package:foodopia/Food_Items/breadsticks.dart';
import 'package:foodopia/Food_Items/chocolava.dart';
import 'package:foodopia/Food_Items/coconutshake.dart';
import 'package:foodopia/Food_Items/margherita_pizza.dart';
import 'package:foodopia/Main_Pages/burgers.dart';
import 'package:foodopia/Main_Pages/cart.dart';
import 'package:foodopia/Main_Pages/drinks.dart';
import 'package:foodopia/Main_Pages/pizzas.dart';
import 'package:foodopia/Main_Pages/restaurants.dart';
import 'package:foodopia/Main_Pages/wraps.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget{
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

TextEditingController searchController = TextEditingController();

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType)
    {
      return SafeArea(
        child: Scaffold(
          drawer: Drawer(),
          body: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    child: AppBar(
                      leading: Builder(builder: (context) => IconButton(onPressed: (){
                        Scaffold.of(context).openDrawer();
                      }, icon: Icon(Icons.sort,color: Colors.black,size: 25,)),),
                      backgroundColor: Colors.white,
                      elevation: 0,
                      actions: [
                        Container(
                            margin: EdgeInsets.only(right: 5.w),
                            child: CircleAvatar(backgroundImage: AssetImage("assets/images/mehulsir.jpg"),))
                      ],
                    ),
                  ),
                  Container(
                    height: 12.h,
                    margin: EdgeInsets.only(left: 5.w,top: 1.h),
                    padding: EdgeInsets.only(top: 2.h),
                    child: Column(
                      children: [
                        Container(
                          height: 5.h,
                          width: double.infinity,
                          child: Text("Find and Order",style: TextStyle(fontSize: 25.sp),),
                          // Text("Food for You",style: TextStyle(fontSize: 25.sp,fontWeight: FontWeight.bold),)
                        ),
                        Container(
                            height: 5.h,
                            width: double.infinity,
                            child: Text("Food for You",style: TextStyle(fontSize: 24.sp,fontWeight: FontWeight.bold),)
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2.h),
                    height: 5.h,
                    width: 85.w,
                    color: Color.fromARGB(69, 206, 206, 206),
                    child: Form(
                      child: TextFormField(
                        controller: searchController,
                        decoration: InputDecoration(
                            hintText: "Search",
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.search,color: Color.fromARGB(255, 229, 164, 2))
                        ),
                      ),

                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 1.h),
                    height: 6.h,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                          child: InkWell(
                            onTap: (){},
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 229, 164, 2),
                                    borderRadius: BorderRadius.all(Radius.circular(10))
                                ),
                                margin: EdgeInsets.only(left: 7.w),
                                width: 12.w,
                                child: Center(child: Text("All",style: TextStyle(color: Colors.white),),)
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Burger()));},
                            child: Container(
                                margin: EdgeInsets.only(left: 4.w),
                                padding: EdgeInsets.only(left: 1.w),
                                width: 25.w,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                  borderRadius: BorderRadius.all(Radius.circular(10)),),
                                child: Center(child: Row(
                                  children: [
                                    Container(
                                        height: 3.h,
                                        child: Image(image: AssetImage("assets/images/burgericon.png"))),
                                    Container(
                                        margin: EdgeInsets.only(left: 1.w),
                                        child: Text("Burgers",style: TextStyle(color: Colors.black),)),
                                  ],
                                ),)
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Wraps()));},
                            child: Container(
                                margin: EdgeInsets.only(left: 2.w),
                                padding: EdgeInsets.only(left: 1.w),
                                width: 25.w,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                  borderRadius: BorderRadius.all(Radius.circular(10)),),
                                child: Center(child: Row(
                                  children: [
                                    Container(
                                        height: 3.h,
                                        child: Image(image: AssetImage("assets/images/wrapicon.png"))),
                                    Container(
                                        margin: EdgeInsets.only(left: 1.w),
                                        child: Text("Wraps",style: TextStyle(color: Colors.black),)),
                                  ],
                                ),)
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Drinks()));},
                            child: Container(
                                margin: EdgeInsets.only(left: 2.w),
                                padding: EdgeInsets.only(left: 1.w),
                                width: 25.w,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                  borderRadius: BorderRadius.all(Radius.circular(10)),),
                                child: Center(child: Row(
                                  children: [
                                    Container(
                                        height: 3.h,
                                        child: Image(image: AssetImage("assets/images/drinksicon.png"))),
                                    Container(
                                        margin: EdgeInsets.only(left: 1.w),
                                        child: Text("Drinks",style: TextStyle(color: Colors.black),)),
                                  ],
                                ),)
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Pizza()));},
                            child: Container(
                                margin: EdgeInsets.only(left: 2.w),
                                padding: EdgeInsets.only(left: 1.w),
                                width: 25.w,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                  borderRadius: BorderRadius.all(Radius.circular(10)),),
                                child: Center(child: Row(
                                  children: [
                                    Container(
                                        height: 3.h,
                                        child: Image(image: AssetImage("assets/images/pizzaicon.png"))),
                                    Container(
                                        margin: EdgeInsets.only(left: 1.w),
                                        child: Text("Pizza",style: TextStyle(color: Colors.black),)),
                                  ],
                                ),)
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 3.h),
                    padding: EdgeInsets.only(right: 65.w),
                    height:4.h,
                    child: Text("Popular",style: TextStyle(fontSize: 20.sp,fontWeight: FontWeight.bold),),
                  ),
                  Container(
                    height: 30.h,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => BlackBurger()));},
                            child: Container(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 1.w),
                              width: 40.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(15)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/blackburger2.jpg'),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 26.w,top: 1.h),
                                    height: 5.h,
                                    child: CircleAvatar(
                                      backgroundColor: Color.fromARGB(255, 229, 164, 2),
                                      child: Center(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                          ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Cart())); },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 13.h,right: 1.w),
                                    height: 9.h,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.white70,
                                    ),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 11.w,top: 1.h,left: 2.w),
                                              child: Text("Black Burger",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w,bottom: 1.h),
                                            child: Row(
                                              children: [
                                                Icon(Icons.currency_rupee,size: 13.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Text("150"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                            child: Container(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 1.w),
                              width: 40.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(15)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/pizza1.jpg'),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 26.w,top: 1.h),
                                    height: 5.h,
                                    child: CircleAvatar(
                                      backgroundColor: Color.fromARGB(255, 229, 164, 2),
                                      child: Center(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                          ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Cart())); },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 13.h,right: 1.w),
                                    height: 9.h,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.white70,
                                    ),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 10.w,top: 1.h,left: 2.w),
                                              child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w,bottom: 1.h),
                                            child: Row(
                                              children: [
                                                Icon(Icons.currency_rupee,size: 13.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Text("260"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => GarlicBreadSticks()));},
                            child: Container(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 1.w),
                              width: 40.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(15)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/garlicbreadstick.jpg'),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 26.w,top: 1.h),
                                    height: 5.h,
                                    child: CircleAvatar(
                                      backgroundColor: Color.fromARGB(255, 229, 164, 2),
                                      child: Center(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                          ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Cart())); },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 13.h,right: 1.w),
                                    height: 9.h,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.white70,
                                    ),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 12.w,top: 1.h,left: 2.w),
                                              child: Text("Bread Sticks",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),


                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w,bottom: 1.h),
                                            child: Row(
                                              children: [
                                                Icon(Icons.currency_rupee,size: 13.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Text("120"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => ChocoLava()));},
                            child: Container(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 1.w),
                              width: 40.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(15)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/chocovolcano.jpg'),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 26.w,top: 1.h),
                                    height: 5.h,
                                    child: CircleAvatar(
                                      backgroundColor: Color.fromARGB(255, 229, 164, 2),
                                      child: Center(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                          ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Cart())); },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 13.h,right: 1.w),
                                    height: 9.h,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.white70,
                                    ),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 11.w,top: 1.h,left: 2.w),
                                              child: Text("Choco Lava Cake",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w,bottom: 1.h),
                                            child: Row(
                                              children: [
                                                Icon(Icons.currency_rupee,size: 13.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Text("99"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h,right: 1.h),
                          child: InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => CoconutShake()));},
                            child: Container(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 1.w),
                              width: 40.w,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(15)),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/Shake.png'),
                                  )),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 26.w,top: 1.h),
                                    height: 5.h,
                                    child: CircleAvatar(
                                      backgroundColor: Color.fromARGB(255, 229, 164, 2),
                                      child: Center(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.add,
                                            color: Colors.white,
                                          ), onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Cart())); },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 13.h,right: 1.w),
                                    height: 9.h,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.white70,
                                    ),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 10.w,top: 1.h,left: 2.w),
                                              child: Text("CoconutShake",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),
                                                Icon(Icons.star,size: 17.sp,color: Colors.grey),



                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.only(right: 10.w,left: 2.w,bottom: 1.h),
                                            child: Row(
                                              children: [
                                                Icon(Icons.currency_rupee,size: 13.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Text("139"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 3.h,left: 4.w),
                    height:4.h,
                    child: Row(
                      children: [
                        Text("Restaurants",style: TextStyle(fontSize: 20.sp,fontWeight: FontWeight.bold),),
                        SizedBox(width: 40.w,),
                        TextButton(
                            onPressed:(){Navigator.push(context, MaterialPageRoute(builder: (context) => Restaurants()));} ,
                            child: Text("See All",style: TextStyle(color: Colors.grey),) )
                      ],
                    ),

                  ),
                  Container(
                    margin: EdgeInsets.only(top: 1.h),
                    height: 15.h,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                          child: InkWell(
                            onTap: (){},
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                              ),
                              margin: EdgeInsets.only(left: 4.w),
                              width: 92.w,
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5.w),
                                    height: 8.h,
                                    width: 20.w,
                                    child: Image(
                                        fit: BoxFit.cover,
                                        image: AssetImage("assets/images/mcd.png")),
                                  ),
                                  Container(
                                    height: 9.h,
                                    margin: EdgeInsets.only(top: 3.h,left: 2.h),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 10.w),
                                              child: Text("McDonald's",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(right: 20.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                          child: Row(
                                            children: [
                                              Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                              Text("3.3 KM"),
                                            ],
                                          ),

                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10.w),
                                    child: Icon(Icons.arrow_forward),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                          child: InkWell(
                            onTap: (){},
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                              ),
                              margin: EdgeInsets.only(left: 4.w),
                              width: 92.w,
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5.w),
                                    height: 8.h,
                                    width: 20.w,
                                    child: Image(
                                        fit: BoxFit.cover,
                                        image: AssetImage("assets/images/pizzahut.png")),
                                  ),
                                  Container(
                                    height: 9.h,
                                    margin: EdgeInsets.only(top: 3.h,left: 2.h),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 10.w),
                                              child: Text("Pizza Hut",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(right: 20.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                          child: Row(
                                            children: [
                                              Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                              Text("2.0 KM"),
                                            ],
                                          ),

                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10.w),
                                    child: Icon(Icons.arrow_forward),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                          child: InkWell(
                            onTap: (){},
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                              ),
                              margin: EdgeInsets.only(left: 4.w),
                              width: 92.w,
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5.w),
                                    height: 8.h,
                                    width: 20.w,
                                    child: Image(
                                        fit: BoxFit.cover,
                                        image: AssetImage("assets/images/dominos.png")),
                                  ),
                                  Container(
                                    height: 9.h,
                                    margin: EdgeInsets.only(top: 3.h,left: 2.h),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              padding: EdgeInsets.only(right: 10.w),
                                              child: Text("Domino's",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(right: 20.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                          child: Row(
                                            children: [
                                              Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                              Text("7.2 KM"),
                                            ],
                                          ),

                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10.w),
                                    child: Icon(Icons.arrow_forward),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                          child: InkWell(
                            onTap: (){},
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                              ),
                              margin: EdgeInsets.only(left: 4.w),
                              width: 92.w,
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 5.w),
                                    height: 8.h,
                                    width: 20.w,
                                    child: Image(
                                        fit: BoxFit.cover,
                                        image: AssetImage("assets/images/dns.jpg")),
                                  ),
                                  Container(
                                    height: 9.h,
                                    margin: EdgeInsets.only(top: 3.h,left: 2.h),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              child: Text("Dessert n' Shakes",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                        ),
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(right: 20.w),
                                            child: Row(
                                              children: [
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                                Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                          child: Row(
                                            children: [
                                              Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                              Text("1.7 KM"),
                                            ],
                                          ),

                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10.w),
                                    child: Icon(Icons.arrow_forward),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

        ),
      );
    });
  }
  Future<http.Response> getDatafromserver() async {
    var response = await http.get(
        Uri.parse('https://63eb9a4f3f583963b2e10df2.mockapi.io/foodItems'));
    print('response ::: $response');
    print(response.body);
    return response;
  }
}
