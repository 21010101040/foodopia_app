import 'package:flutter/material.dart';
import 'package:foodopia/Main_Pages//home.dart';
import 'package:foodopia/Main_Pages//signin.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:flutter/src/widgets/form.dart';
import 'package:form_validator/form_validator.dart';

class Login extends StatefulWidget {
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void validate(){
    if(formKey.currentState!.validate()){
      print("Ok");
    }
    else{
      print("Error");
    }
  }

  TextEditingController useridController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  String userIdErrorText = "Please Write UserID";

  String passwordErrorText = "Please Enter Password";

  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
    return SafeArea(
        child:Scaffold(
          body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                opacity: 0.8,
                fit: BoxFit.cover,
                  image: AssetImage("assets/images/Pizza.png"))
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(top: 20.h),
                      height: 56.h,
                      width: 80.w,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 4.h),
                              height:10.h,
                              width:25.h,
                              child: Center(child: Text("Foodopia.in",style: TextStyle(fontSize: 25.sp,fontWeight: FontWeight.w400,fontFamily: "Lobster-Regular",color: Colors.black),)),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 4.h),
                              height: 35.h,
                              width: double.infinity,
                              child: Form(
                                key: formKey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 8.h,
                                      width: 75.w,
                                      decoration: BoxDecoration(
                                        color: Colors.white70,
                                        border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                        borderRadius: BorderRadius.all(Radius.circular(50)),
                                      ),
                                      child: Center(
                                        child: TextFormField(
                                          controller: useridController,
                                          validator: ValidationBuilder().email().maxLength(100).build(),
                                          decoration: InputDecoration(
                                            labelText: "Enter User Id",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.person,color: Color.fromARGB(255, 229, 164, 2))
                                          ),

                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                      height: 8.h,
                                      width: 75.w,
                                      decoration: BoxDecoration(
                                        color: Colors.white70,
                                        border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                        borderRadius: BorderRadius.all(Radius.circular(50)),
                                      ),
                                      child: Center(
                                        child: TextFormField(
                                          controller: passwordController,
                                          validator: (password){
                                            if (password!.isEmpty){
                                              return passwordErrorText;
                                            }
                                            else if(password.length<8){
                                              return "At least 8 character required";

                                            }
                                            else{
                                              return null;
                                            }
                                          },
                                          obscureText: visible? false : true,
                                          decoration: InputDecoration(
                                              labelText: "Enter Password",
                                              border: InputBorder.none,
                                              prefixIcon: Icon(Icons.lock,color: Color.fromARGB(255, 229, 164, 2)),
                                              suffixIcon: IconButton(
                                                  icon: visible?
                                                  Icon(
                                                      Icons.visibility,
                                                      color: Color.fromARGB(255, 229, 164, 2)):
                                                  Icon(
                                                      Icons.visibility_off,
                                                      color: Color.fromARGB(255, 229, 164, 2)),
                                                onPressed: () {
                                                    setState(() {
                                                      visible = !visible;
                                                    });
                                                },)
                                          ),

                                        ),
                                      ),
                                    ),
                                    SingleChildScrollView(
                                      child: Container(
                                        height: 10.h,
                                        child: Column(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(top:1.h),
                                              height: 5.h,
                                              width: 50.w,
                                              decoration: BoxDecoration(
                                                color: Color.fromARGB(255, 229, 164, 2),
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                              ),
                                              child: ElevatedButton(
                                                  style: ButtonStyle(
                                                      shape: MaterialStateProperty.all(
                                                          RoundedRectangleBorder(
                                                              borderRadius: BorderRadius.circular(10)
                                                          )
                                                      ),
                                                      backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 229, 164, 2))
                                                  ),
                                                  onPressed: () {
                                                    if (formKey.currentState!.validate()) {
                                                      ScaffoldMessenger.of(context).showSnackBar(
                                                        const SnackBar(content: Text('Logging In')),
                                                      );
                                                      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                                                    }
                                                  },
                                                  child: Center(
                                                      child: Text("Login",style: TextStyle(color: Colors.white),)
                                                  )
                                              ),
                                            ),
                                            Container(
                                              height: 4.h,
                                              padding: EdgeInsets.only(left: 19.w),
                                              child: Row(

                                                children: [
                                                  Text("Don't have account?",style: TextStyle(fontSize: 14.sp),),
                                                  TextButton(
                                                      onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));},
                                                      child: Text("Sign Up",style: TextStyle(fontSize: 14.sp,color: Color.fromARGB(255, 229, 164, 2)),))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )) ;
    });
  }
}