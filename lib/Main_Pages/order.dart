import 'package:flutter/material.dart';

class Order extends StatelessWidget{
  const Order({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.orangeAccent,
        child: Center(child: Text("Order Placed",style: TextStyle(fontWeight: FontWeight.bold),)),
      ),
    );
  }

}