import 'package:flutter/material.dart';
import 'package:foodopia/Food_Items/margherita_pizza.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class Pizza extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.only(right: 50.w),
              height: 6.h,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Color.fromARGB(255, 229, 164, 2),
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Center(child: Text("Pizza",style: TextStyle(fontSize: 25.sp,fontWeight: FontWeight.bold),)),
            ),
            Container(
              margin: EdgeInsets.only(right: 3.w),
              height: 89.h,
              width: 90.w,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MargheritaPizza()));},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        margin: EdgeInsets.only(left: 4.w),
                        width: 92.w,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.w),
                              height: 8.h,
                              width: 20.w,
                              child: Image(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/images/pizza1.jpg")),
                            ),
                            Container(
                              height: 9.h,
                              margin: EdgeInsets.only(top: 3.h,left: 1.w),
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                        padding: EdgeInsets.only(left: 2.w),
                                        child: Text("Margherita Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 15.w),
                                      child: Row(
                                        children: [
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                          Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                                    child: Row(
                                      children: [
                                        Icon(Icons.currency_rupee,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                        Text("260"),
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.w),
                              child: Icon(Icons.arrow_forward),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}