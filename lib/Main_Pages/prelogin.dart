import 'package:flutter/material.dart';
import 'package:foodopia/Main_Pages//login.dart';
import 'package:foodopia/Main_Pages//signin.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class PreLogin extends StatelessWidget{
  const PreLogin({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return SafeArea(
        child: Scaffold(
          body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Prelogin.png')
                )
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 53.h, right: 22.w),

                    child: Column(
                      children: [
                        Text("Quick Delivery", style: TextStyle(fontSize: 25.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),),
                        Text("at Your Doorstep", style: TextStyle(fontSize: 23.sp,
                            color: Color.fromARGB(255, 229, 164, 2),
                            fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 2.h, right: 26.w),
                    child: Column(
                      children: [
                        Text(
                          "Home delivery and online reservation\nsystem for restaurants and cafe",
                          style: TextStyle(fontSize: 15.sp, color: Colors.white),),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5.h),
                    child: Column(
                      children: [
                        Container(
                          height: 7.h,
                          width: 80.w,
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 229, 164, 2),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(10)
                                      )
                                  ),
                                  backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 229, 164, 2))
                              ),
                              onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));},
                              child: Center(
                                  child: Text("Login",style: TextStyle(color: Colors.white),)
                              )
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 2.h),
                          height: 7.h,
                          width: 80.w,
                          decoration: BoxDecoration(
                            color: Colors.black,
                            border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(10),
                                      )
                                  ),
                                  backgroundColor: MaterialStatePropertyAll(Colors.black)
                              ),
                              onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));},
                              child: Center(
                                  child: Text("Sign Up",style: TextStyle(color: Colors.white),)
                              )
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      );
    } );
  }

}