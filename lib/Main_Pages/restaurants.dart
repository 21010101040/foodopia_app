import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class Restaurants extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 1.h,left: 3.w),
        width: 90.w,
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/mcd.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text("McDonald's",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("3.3 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/pizzahut.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text("Pizza Hut",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("2.0 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/dominos.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text("Domino's",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("7.2 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/dns.jpg")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  child: Text("Dessert n' Shakes",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("1.7 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/subway.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.only(right: 16.w),
                                  child: Text("Subway",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("5.2 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/burgerking.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text("Burger King",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("6.4 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/lppizza.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 5.w),
                                  child: Text("La Pino'z Pizza",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("0.7 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/baskinr.png")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 5.w),
                                  child: Text("Baskin Robbins",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("5.0 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/coffeecult.jpg")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.only(right: 5.w),
                                  child: Text("Coffee Culture",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("2.5 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/chatkazz.jpg")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 15.w),
                                  child: Text("Chatkazz",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("3.6 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 1.h,bottom: 1.h),
              child: InkWell(
                onTap: (){},
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  margin: EdgeInsets.only(left: 4.w),
                  width: 92.w,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.w),
                        height: 8.h,
                        width: 20.w,
                        child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/yofrankie.jpg")),
                      ),
                      Container(
                        height: 9.h,
                        margin: EdgeInsets.only(top: 3.h,left: 2.h),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                  padding: EdgeInsets.only(right: 12.w),
                                  child: Text("Yo Frankie",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17.sp),)),
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 20.w),
                                child: Row(
                                  children: [
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                    Icon(Icons.star,size: 14.sp,color: Colors.grey),

                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 2.h,right: 20.w),
                              child: Row(
                                children: [
                                  Icon(Icons.location_on,size: 15.sp,color: Color.fromARGB(255, 229, 164, 2),),
                                  Text("4.8 KM"),
                                ],
                              ),

                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.w),
                        child: Icon(Icons.arrow_forward),
                      )
                    ],
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

}