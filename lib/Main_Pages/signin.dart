import 'package:flutter/material.dart';
import 'package:foodopia/Main_Pages//home.dart';
import 'package:form_validator/form_validator.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class SignIn extends StatefulWidget {
  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void validate(){
    if(formKey.currentState!.validate()){
      print("Ok");
    }
    else{
      print("Error");
    }
  }

  TextEditingController fnameController = TextEditingController();

  TextEditingController lnameController = TextEditingController();

  TextEditingController mobileController = TextEditingController();

  TextEditingController useridController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController cpasswordController = TextEditingController();

  String fnameErrorText = "Please Write First Name";

  String lnameErrorText = "Please Write Last Name";

  String mobileErrorText = "Please Write Mobile Number";

  String userIdErrorText = "Please Write UserID";

  String passwordErrorText = "Please Enter Password";

  String cpasswordErrorText = "Please Confirm Password";

  bool visiblep = false;
  bool visiblecp = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return SafeArea(
          child:Scaffold(
            body: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 0.8,
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/Shake.png"))
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 13.h),
                      height: 70.h,
                      width: 80.w,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 6.h,left: 10.w,right: 10.w),
                            height:10.h,
                            width:90.w,
                            child: Center(child: Text("Welcome to Foodopia",style: TextStyle(fontSize: 20.sp,fontWeight: FontWeight.w400,fontFamily: "Lobster-Regular",color: Colors.black),)),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 1.h,bottom: 4.h),
                            height: 60.h,
                            width: double.infinity,
                            child: Form(
                              key: formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: fnameController,
                                        validator: (fname){
                                          if (fname!.isEmpty){
                                            return fnameErrorText;
                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        decoration: InputDecoration(
                                            hintText: "Enter First Name",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.person,color: Color.fromARGB(255, 229, 164, 2))
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: lnameController,
                                        validator: (lname){
                                          if (lname!.isEmpty){
                                            return passwordErrorText;
                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        decoration: InputDecoration(
                                            hintText: "Enter Last Name",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.person,color: Color.fromARGB(255, 229, 164, 2))
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: useridController,
                                        validator: ValidationBuilder().email().maxLength(100).build(),
                                        decoration: InputDecoration(
                                            hintText: "Enter Mail-Id",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.mail,color: Color.fromARGB(255, 229, 164, 2))
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: mobileController,
                                        validator: (mobile){
                                          if (mobile!.isEmpty){
                                            return mobileErrorText;
                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        decoration: InputDecoration(
                                            hintText: "Enter Mobile Number",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.phone,color: Color.fromARGB(255, 229, 164, 2))
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: passwordController,
                                        validator: (password){
                                          if (password!.isEmpty){
                                            return passwordErrorText;
                                          }
                                          else if(password.length<8){
                                            return "At least 8 character required";

                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        obscureText: visiblep? false : true,
                                        decoration: InputDecoration(
                                            hintText: "Enter Password",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.lock,color: Color.fromARGB(255, 229, 164, 2)),
                                            suffixIcon: IconButton(
                                              icon: visiblep?
                                              Icon(
                                                  Icons.visibility,
                                                  color: Color.fromARGB(255, 229, 164, 2)):
                                              Icon(
                                                  Icons.visibility_off,
                                                  color: Color.fromARGB(255, 229, 164, 2)),
                                              onPressed: () {
                                                setState(() {
                                                  visiblep = !visiblep;
                                                });
                                              },)
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 1.h,bottom: 1.h),
                                    height: 5.h,
                                    width: 75.w,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      border: Border.all(color: Color.fromARGB(255, 229, 164, 2)),
                                      borderRadius: BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        controller: cpasswordController,
                                        validator: (cpassword){
                                          if (cpassword!.isEmpty){
                                            return passwordErrorText;
                                          }
                                          else if (cpassword == passwordController){
                                            return "Confirm your Password";

                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        obscureText: visiblecp? false : true,
                                        decoration: InputDecoration(
                                            hintText: "Confirm Password",
                                            border: InputBorder.none,
                                            prefixIcon: Icon(Icons.lock,color: Color.fromARGB(255, 229, 164, 2)),
                                            suffixIcon: IconButton(
                                              icon: visiblecp?
                                              Icon(
                                                  Icons.visibility,
                                                  color: Color.fromARGB(255, 229, 164, 2)):
                                              Icon(
                                                  Icons.visibility_off,
                                                  color: Color.fromARGB(255, 229, 164, 2)),
                                              onPressed: () {
                                                setState(() {
                                                  visiblecp = !visiblecp;
                                                });
                                              },)
                                        ),

                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 4.h),
                                    height: 5.h,
                                    width: 50.w,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(255, 229, 164, 2),
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                    ),
                                    child: ElevatedButton(
                                        style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(10)
                                                )
                                            ),
                                            backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 229, 164, 2)),
                                        ),
                                        onPressed: () {
                                          if (formKey.currentState!.validate()) {
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                                          }
                                        },
                                        child: Center(
                                            child: Text("Sign-In",style: TextStyle(color: Colors.white),)
                                        )
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )) ;
    });
  }
}