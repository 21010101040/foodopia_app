import 'dart:async';
import 'package:flutter/material.dart';
import 'package:foodopia/Main_Pages//prelogin.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class Splash extends StatefulWidget{
  const Splash({super.key});

  @override
  State<Splash> createState() => SplashScreen();
}
class SplashScreen extends State<Splash> {
  @override
  void initState() {
    Timer(const Duration(seconds: 4), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (_) => const PreLogin()),
              (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
    return SafeArea(
      child: Scaffold(
          body: Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 229, 164, 2)
            ),
            child: Center(
              child: Container(
                child: Text("Foodopia.in",style: TextStyle(fontSize: 30.sp,fontFamily: "Lobster-Regular"),),
              ),
            ),
          )
      ),
    );
    } );
  }
}