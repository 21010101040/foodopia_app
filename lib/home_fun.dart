import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'Main_Pages/cart.dart';

Widget listofItems(BuildContext context,{required String imageURL,required Widget Function(BuildContext) navigation,required String title,required String cost}) {
  return ResponsiveSizer(builder: (context, orientation, screenType) {
    return Padding(
      padding: EdgeInsets.only(top: 1.h, bottom: 1.h, right: 1.h),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: navigation));
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.only(left: 4.w),
          padding: EdgeInsets.only(left: 1.w),
          width: 40.w,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage('${imageURL}'),
              )),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 26.w, top: 1.h),
                height: 5.h,
                child: CircleAvatar(
                  backgroundColor: Color.fromARGB(255, 229, 164, 2),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.add,
                        color: Colors.white,
                      ), onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) =>const Cart()));
                    },
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 13.h, right: 1.w),
                height: 9.h,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white70,
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.only(
                              right: 11.w, top: 1.h, left: 2.w),
                          child: Text("${title}", style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.sp),)),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 10.w, left: 2.w),
                        child: Row(
                          children: [
                            Icon(Icons.star, size: 17.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),
                            Icon(Icons.star, size: 17.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),
                            Icon(Icons.star, size: 17.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),
                            Icon(Icons.star, size: 17.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),
                            Icon(Icons.star, size: 17.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),

                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(
                            right: 10.w, left: 2.w, bottom: 1.h),
                        child: Row(
                          children: [
                            Icon(Icons.currency_rupee, size: 13.sp,
                              color: Color.fromARGB(255, 229, 164, 2),),
                            Text("${cost}"),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  );
}